#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <Windows.h>
#include <KnownFolders.h>
#include <ShlObj.h>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Filter the file system model and set its order
    m_file_model.setFilter( QDir::Files | QDir::NoDotAndDotDot );
    m_file_model.sort( 1, Qt::SortOrder::DescendingOrder ); // Sort by size

    // Connect the model to the list view
    ui->li_source->setModel( &m_file_model );

    // Connect the signal to preview the items as they are selected on the list view
    connect( ui->li_source->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection))
           , this, SLOT(previewSelection(QItemSelection)) );

    // Find the source and default destination folders
    if ( findSourceFolder() ) {
        showStatus( tr("Ready") );
    } else {
        showStatus( tr("ERROR: Spotlight source folder not found.") );
    }

    findDestinationFolder();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showStatus( QString status )
{
    ui->statusBar->showMessage( status );
}

bool MainWindow::findSourceFolder()
{
    // Let's find out the appdata folder where the Spotlight assets are located
    // TODO: Maybe in the future it would be nice to have something that looked in different folders
    PWSTR path;

    // Look for the local appdata folder
    if ( SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, nullptr, &path) == S_OK ) {

        // Found. Convert the path to a QString
        QString assets_folder_name = QString::fromWCharArray(path);

        // And add the location of the Spotlight assets folder
        assets_folder_name.append("/Packages/Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy/LocalState/Assets");

        // Now, let's set the root of both the model and the list view
        m_file_model.setRootPath( assets_folder_name );
        ui->li_source->setRootIndex( m_file_model.index(assets_folder_name) );

        // Finally, free the memory taken up by the SHGetKnownFolderPath
        CoTaskMemFree( path );

        return true;
    }

    return false;
}

void MainWindow::findDestinationFolder()
{
    PWSTR path;

    // Look for this user's Pictures folder
    if ( SHGetKnownFolderPath(FOLDERID_Pictures, 0, nullptr, &path) == S_OK ) {
        setDestinationFolder( QString::fromWCharArray(path) );
    }

    CoTaskMemFree( path );
}

void MainWindow::setDestinationFolder( QString folder )
{
    // If we can change to the folder, show it on the edit, if not, clear.
    if ( m_destination_folder.setCurrent( folder ) ) {
        ui->ed_destination_folder->setText( QDir::toNativeSeparators( m_destination_folder.currentPath() ) );
    } else {
        ui->ed_destination_folder->clear();
    }
}

void MainWindow::on_bt_select_destination_folder_clicked()
{
    // Let the user select a different destination folder
    setDestinationFolder( QFileDialog::getExistingDirectory( this, tr( "Select destination folder" ) ) );
}

void MainWindow::previewSelection(const QItemSelection& selection)
{
    // If there's something selected
    if ( !selection.indexes().isEmpty() ) {

        // Get the index of the first item select, and from that the filename with its complete path
        QModelIndex index = selection.indexes().first();

        m_selected_file = m_file_model.filePath( index );

        // Try to open that file as an image.
        QPixmap image( m_selected_file );

        if ( !image.isNull() ) {
            // We got an image!

            // Get the image dimensions
            ui->lb_image_size->setText( QString::number( image.width() ).append( "x" ).append( QString::number( image.height() ) ) );

            // Let's hightlight the size if the aspect ratio is less than 1.7
            if ( static_cast<float>( image.width() ) / static_cast<float>( image.height() ) < 1.7f ) {
                ui->lb_image_size->setStyleSheet("QLabel { background-color : red; color : black; }");
            } else {
                ui->lb_image_size->setStyleSheet("");
            }

            // Set the preview
            ui->lb_preview->setPixmap( image );

            // Get the filename for the default destination
            ui->ed_destination_filename->setText( m_file_model.fileName( index ) );

            // Let's allow the user to copy it
            ui->bt_copy->setEnabled(true);

            showStatus( tr("Valid file.") );
            return;
        }
    }

    // If we got here, it's because this isn't a good file, so let's clear it all
    ui->lb_image_size->clear();
    ui->lb_preview->clear();
    ui->ed_destination_filename->clear();
    ui->bt_copy->setEnabled(false);
    m_selected_file.clear();
    showStatus( tr("Invalid file.") );
}


void MainWindow::on_bt_copy_clicked()
{
    // Let's be sure we can try to copy this file...
    if ( !ui->bt_copy->isEnabled() ) return;
    if ( m_selected_file.isEmpty() ) return;

    // If there's no destination folder, ask the user, and if s/he is stubborn, give up
    if ( ui->ed_destination_folder->text().isEmpty() ) {
        on_bt_select_destination_folder_clicked();

        if ( ui->ed_destination_folder->text().isEmpty() ) {
            showStatus( tr("Select a destination folder.") );
            return;
        }
    }

    // Get the destination filename. We need one.
    QString destinationFilename = ui->ed_destination_filename->text();

    if ( destinationFilename.isEmpty() || destinationFilename.isNull() ) {
        showStatus( tr("Enter a filename.") );
        return;
    }

    // Finally, copy the file
    if ( QFile::copy( m_selected_file
                      , m_destination_folder.currentPath()
                      .append( "/" )
                      .append( destinationFilename )
                      .append( ".jpg" ) ) ) {
        showStatus( tr("File copied.") );
    } else {
        showStatus( tr("ERROR copying file.") );
    }
}
