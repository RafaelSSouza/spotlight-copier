#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QItemSelection>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void previewSelection(const QItemSelection& selection);

private slots:
    void on_bt_select_destination_folder_clicked();

    void on_bt_copy_clicked();

private:
    Ui::MainWindow *ui;

    QFileSystemModel m_file_model;

    QDir m_destination_folder;
    QString m_selected_file;

    void showStatus( QString status );

    bool findSourceFolder();
    void findDestinationFolder();

    void setDestinationFolder( QString folder );
};

#endif // MAINWINDOW_H
